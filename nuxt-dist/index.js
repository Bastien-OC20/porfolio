import Vue from 'vue'

import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from './components/nuxt-error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'

/* Plugins */

import nuxt_plugin_plugin_2df8b7bb from 'nuxt_plugin_plugin_2df8b7bb' // Source: .\\components\\plugin.js (mode: 'all')
import nuxt_plugin_image_8acead4e from 'nuxt_plugin_image_8acead4e' // Source: .\\image.js (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Object.defineProperty(Vue.prototype, '$nuxt', {
  get() {
    const globalNuxt = this.$root.$options.$nuxt
    if (process.client && !globalNuxt && typeof window !== 'undefined') {
      return window.$nuxt
    }
    return globalNuxt
  },
  configurable: true
})

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":true,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp(ssrContext, config = {}) {
  const router = await createRouter(ssrContext, config)

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"title":"Sébastien Rapuzzi | Développeur web frontend, à votre service !","htmlAttrs":{"lang":"fr"},"meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1","ie":"edge"},{"name":"Author","content":"Sébastien Rapuzzi"},{"name":"Publisher","content":"Vue.js + Nuxt.js"},{"name":"theme-color","content":"#000"},{"property":"og:url","content":"https:\u002F\u002Frands.netlify.app\u002F"},{"property":"og:type","content":"website"},{"property":"og:title","content":"R&S - Web | e-portfolio de Sebastien Rapuzzi"},{"name":"description","content":"Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS"},{"property":"og:description","content":"Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS"},{"name":"twitter:card","content":"website"},{"name":"twitter:site","content":"@S3baast1en"},{"name":"twitter:creator","content":"@S3baast1en"},{"name":"twitter:title","content":"R&S - Web | e-portfolio de Sebastien Rapuzzi"},{"name":"twitter:description","content":"Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS"},{"name":"twitter:image","content":"https:\u002F\u002Frands.netlify.app\u002Flogo-rands.png"},{"name":"google-site-verification","content":"2wGkRx0bGhKXBm2TJGiUxICEtrstf13Ohpgz-oYkdFQ"},{"name":"keywords","content":"R and S - web, Marseille, Aubagne, la penne sur huveaune, Gémenos, Pont de l'étoile, bouches du rhône, PACA, Portfolio de Sébastien Rapuzzi, developpeur Web frontend junior à marseille et à ses environs, Développement, front-end, backend , backend, site internet, site web, web , internet, CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS, developpeur freelance, développeur web freelance, developpement web, trouver un développeur web, cherche développeur web, site web developpeur, recherche développeur web freelance, freelance dev web, mission freelance developpeur web, développeur web indépendant, mission, freelance, SEO, OWASP, RGDP, WCAG, W3C, Lighthouse "},{"hid":"description","name":"description","content":""}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":".\u002Ffavicon.ico"},{"rel":"canonical","href":"https:\u002F\u002Frands.netlify.app\u002F"},{"rel":"apple-touch-icon","href":".\u002Ffavicon.ico"},{"rel":"StyleSheet","href":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002Fbootstrap@5.0.2\u002Fdist\u002Fcss\u002Fbootstrap.min.css"},{"rel":"StyleSheet","href":"https:\u002F\u002Funpkg.com\u002Faos@2.3.1\u002Fdist\u002Faos.css"}],"script":[{"src":"https:\u002F\u002Fcdnjs.cloudflare.com\u002Fajax\u002Flibs\u002Fjquery\u002F3.3.1\u002Fjquery.min.js","body":true},{"src":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002Fbootstrap@5.0.2\u002Fdist\u002Fjs\u002Fbootstrap.bundle.min.js","body":true},{"src":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002F@popperjs\u002Fcore@2.9.2\u002Fdist\u002Fumd\u002Fpopper.min.js","body":true},{"src":"https:\u002F\u002Funpkg.com\u002Faos@2.3.1\u002Fdist\u002Faos.js","body":true}],"style":[]},

    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_plugin_2df8b7bb === 'function') {
    await nuxt_plugin_plugin_2df8b7bb(app.context, inject)
  }

  if (typeof nuxt_plugin_image_8acead4e === 'function') {
    await nuxt_plugin_image_8acead4e(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // Wait for async component to be resolved first
  await new Promise((resolve, reject) => {
    // Ignore 404s rather than blindly replacing URL in browser
    if (process.client) {
      const { route } = router.resolve(app.context.route.fullPath)
      if (!route.matched.length) {
        return resolve()
      }
    }
    router.replace(app.context.route.fullPath, resolve, (err) => {
      // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
      if (!err._isRouter) return reject(err)
      if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

      // navigated to a different route in router guard
      const unregister = router.afterEach(async (to, from) => {
        if (process.server && ssrContext && ssrContext.url) {
          ssrContext.url = to.fullPath
        }
        app.context.route = await getRouteData(to)
        app.context.params = to.params || {}
        app.context.query = to.query || {}
        unregister()
        resolve()
      })
    })
  })

  return {
    app,
    router
  }
}

export { createApp, NuxtError }
