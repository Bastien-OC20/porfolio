# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Contact>` | `<contact>` (components/Contact.vue)
- `<Intro>` | `<intro>` (components/Intro.vue)
- `<Projets>` | `<projets>` (components/Projets.vue)
- `<Skill>` | `<skill>` (components/Skill.vue)
- `<Student>` | `<student>` (components/Student.vue)
- `<ImgIntro>` | `<img-intro>` (components/img/ImgIntro.vue)
