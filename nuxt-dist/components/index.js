export { default as Contact } from '../..\\components\\Contact.vue'
export { default as Intro } from '../..\\components\\Intro.vue'
export { default as Projets } from '../..\\components\\Projets.vue'
export { default as Skill } from '../..\\components\\Skill.vue'
export { default as Student } from '../..\\components\\Student.vue'
export { default as ImgIntro } from '../..\\components\\img\\ImgIntro.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
