export default {
 // Target: https://go.nuxtjs.dev/config-target
 ssr: false,
 target: 'static',
 static: {
   prefix: false
 },
 router: {
   base: '/'
 },
 buildDir: 'nuxt-dist',

 // Global page headers: https://go.nuxtjs.dev/config-head
 head: {
   title: 'Sébastien Rapuzzi | Développeur web frontend, à votre service !',
   htmlAttrs: {
     lang: 'fr'
   },

   /*---------------------- Config Meta --------------------------------------*/
   meta: [
     { charset: 'utf-8' },
     {
       name: 'viewport',
       content: 'width=device-width, initial-scale=1',
       ie: 'edge'
     },
     { name: 'Author', content: 'Sébastien Rapuzzi' },
     { name: 'Publisher', content: 'Vue.js + Nuxt.js' },
     { name: 'theme-color', content: '#000' },
     {
       property: 'og:url',
       content: 'https://rands.netlify.app/'
     },
     { property: 'og:type', content: 'website' },
     {
       property: 'og:title',
       content: 'R&S - Web | e-portfolio de Sebastien Rapuzzi'
     },
     {
       name: 'description',
       content:
         'Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS'
     },
     {
       property: 'og:description',
       content:
         'Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS'
     },
     { name: 'twitter:card', content: 'website' },
     { name: 'twitter:site', content: '@S3baast1en' },
     { name: 'twitter:creator', content: '@S3baast1en' },
     {
       name: 'twitter:title',
       content: 'R&S - Web | e-portfolio de Sebastien Rapuzzi'
     },
     {
       name: 'twitter:description',
       content:
         'Portfolio de Sébastien Rapuzzi | developpeur Web frontend junior à marseille et à ses environs. Développement en CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS'
     },
     {
       name: 'twitter:image',
       content:
         'https://rands.netlify.app/logo-rands.png'
     },
     // google search console
     {
       name: 'google-site-verification',
       content: '2wGkRx0bGhKXBm2TJGiUxICEtrstf13Ohpgz-oYkdFQ'
     },
     {
       name: 'keywords',
       content:
         'R and S - web, Marseille, Aubagne, la penne sur huveaune, Gémenos, Pont de l\'étoile, bouches du rhône, PACA, Portfolio de Sébastien Rapuzzi, developpeur Web frontend junior à marseille et à ses environs, Développement, front-end, backend , backend, site internet, site web, web , internet, CSS3, Html5, SaSS, Framework Boostrap, JavaScript, Angular.JS, Express.JS, Node.JS, NPM, Vue.JS et Nuxt.JS, developpeur freelance, développeur web freelance, developpement web, trouver un développeur web, cherche développeur web, site web developpeur, recherche développeur web freelance, freelance dev web, mission freelance developpeur web, développeur web indépendant, mission, freelance, SEO, OWASP, RGDP, WCAG, W3C, Lighthouse '
     },
     { hid: 'description', name: 'description', content: '' }
   ],
   /*----------------------Config Link --------------------------------------*/
   link: [
     { rel: 'icon', type: 'image/x-icon', href: './favicon.ico' },
     {
       rel: 'canonical',
       href: 'https://rands.netlify.app/'
     },
     { rel: 'apple-touch-icon', href: './favicon.ico' },
     {
       rel: 'StyleSheet',
       href:
         'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css'
     },
     {
       rel: 'StyleSheet',
       href:'https://unpkg.com/aos@2.3.1/dist/aos.css'
     },
     
   ],
     /*----------------------Config Script --------------------------------------*/
   script: [
     { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', body: true },
     { src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js', body: true },
     { src: 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js', body: true },
     { src: 'https://unpkg.com/aos@2.3.1/dist/aos.js', body: true },


   ]
 },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/style-resources',
    '@nuxt/image',
  ],
  styleResources: {
    scss: [
      '@/assets/scss/mixins.scss',
      '@/assets/scss/variables.scss'
    ]
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  rules: [
    // ... other rules omitted
  
    // this will apply to both plain `.scss` files
    // AND `<style lang="scss">` blocks in `.vue` files
    {
      test: /\.scss$/,
      use: [
        'vue-style-loader',
        'css-loader',
        'sass-loader'
      ]
    }
  ]
}
